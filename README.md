Fib
===

## Fibonacci web service

The web service accepts a number, n, as input and returns the first n Fibonacci numbers, starting from 0. i.e. given n = 5, appropriate output would represent the sequence "[0, 1, 1, 2, 3]".

Install with:
```
python setup.py install
```

Run server with:
```
gunicorn fib.app:app
```

To query, where n = 5.
```
curl -i http://localhost:8000/fibs/5
```

Run tests with:
```
pip install tox
tox
```

Note regarding performance: the request times out (at a default timeout of 30 seconds) somewhere between n = 40,000 and 50,000 on my local machine.
