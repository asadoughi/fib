from setuptools import find_packages, setup

setup(
    name='fib',
    version='0.1',
    description='Fibonacci web service',
    author='Amir Sadoughi',
    author_email='me@princehonest.com',
    install_requires=['flask-restful', 'gunicorn'],
    packages=find_packages(exclude=['tests']),
)
