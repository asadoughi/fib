from flask import Flask
from flask_restful import Api
from fib.common import exceptions
from fib.resources.fibs import Fibs


app = Flask(__name__)
api = Api(app, errors=exceptions.errors, catch_all_404s=True)

route = "/fibs/<string:length>"
api.add_resource(Fibs, route)
