from flask_restful import Resource
from fib.common import exceptions


class Fibs(Resource):
    def get(self, length):
        length = int(length)
        if length < 0:
            raise exceptions.BadFibonacciLength()
        result = []
        a = 0
        b = 1
        for i in xrange(length):
            result.append(a)
            a, b = a + b, a
        return result
