class BadFibonacciLength(Exception):
    pass


errors = {
    "BadFibonacciLength": {
        "status": "400",
        "message": "length must be non-negative"
    },
    "ValueError": {
        "status": "400",
        "message": "length must be an integer"
    },
}
