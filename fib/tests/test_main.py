import json
import unittest

from fib import app


class TestBase(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()


class TestMain(TestBase):
    def test_get_fibs(self):
        rv = self.app.get("/fibs")
        self.assertEqual(rv.status_code, 404)
        body = json.loads(rv.data)
        self.assertIn("did you mean /fibs/<string:length>", body["message"])

    def test_get_fibs_noninteger(self):
        rv = self.app.get("/fibs/this-is-not-an-integer")
        self.assertEqual(rv.status_code, 400)
        body = json.loads(rv.data)
        self.assertEqual(body["message"], "length must be an integer")

    def test_get_fibs_negative(self):
        rv = self.app.get("/fibs/-5")
        self.assertEqual(rv.status_code, 400)
        body = json.loads(rv.data)
        self.assertEqual(body["message"], "length must be non-negative")

    def test_get_fibs_zero(self):
        rv = self.app.get("/fibs/0")
        self.assertEqual(rv.status_code, 200)
        body = json.loads(rv.data)
        self.assertEqual(body, [])

    def test_get_fibs_one(self):
        rv = self.app.get("/fibs/1")
        self.assertEqual(rv.status_code, 200)
        body = json.loads(rv.data)
        self.assertEqual(body, [0])

    def test_get_fibs_two(self):
        rv = self.app.get("/fibs/2")
        self.assertEqual(rv.status_code, 200)
        body = json.loads(rv.data)
        self.assertEqual(body, [0, 1])

    def test_get_fibs_five(self):
        rv = self.app.get("/fibs/5")
        self.assertEqual(rv.status_code, 200)
        body = json.loads(rv.data)
        self.assertEqual(body, [0, 1, 1, 2, 3])
